#!/bin/sh
 
BLACKLISTS="
https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
"
 
REDIRECT_TARGET="127.0.0.1"

TMP_FILE=/tmp/dns-blacklist-$(date +%s)
for LIST in $BLACKLISTS;
do
    echo "fetching $LIST"
    curl -sS $LIST >> $TMP_FILE
done

echo "creating unbound file with redirects to $REDIRECT_TARGET"
grep '^0\.0\.0\.0' $TMP_FILE | awk -v redirect=$REDIRECT_TARGET '{if ($2!="0.0.0.0") print "local-zone: \""$2"\" redirect\nlocal-data: \""$2" A "redirect"\""}' > /var/unbound/ad-blacklist.conf

chown unbound:unbound /var/unbound/ad-blacklist.conf

num=$(cat /var/unbound/ad-blacklist.conf | wc -l)
echo "blocking $(expr $num / 2) domains"

echo "restart unbound"
# pluginctl dns
